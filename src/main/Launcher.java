package main;

/**
 * 这是项目DenvosBootMonitor的主类.
 * 关于更多信息,请参阅:<a href="https://gitlab.com/Denvo/denvosbootmonitor">GitLab</a>
 * @author Denvo Zonis
 * @version 0.0.1
 */
public class Launcher { public static void main(String[] args) {new Main();}}