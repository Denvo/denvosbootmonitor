已弃坑.

# DenvosBootMonitor
![Icon](https://gitlab.com/Denvo/denvosbootmonitor/-/raw/main/DenvosBootMonitor.png)
***
这是一款能够记录时间等信息的一款小程序。

启动程序后，程序将会自动把启动程序时计算机的一些信息保存下来。

是的，功能只有这么简单。

作者不打算长久更新此项目，但是有任何问题，可以发issue。

如果想了解使用方法，请查阅Wiki。
