package main;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.SafeConstructor;

import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

public class Main {
    private final String JarPath = new File(Launcher.class.getProtectionDomain().getCodeSource().getLocation().getPath()).getParent() +"/";
    public Main() {
        LocalDateTime dateTime = LocalDateTime.now();
        Map<String,Object> config = getConfig();
        String fileName = getFileName((String) config.get("FileName"),dateTime);
        saveFile(getInformation(dateTime,config),getFilePath((String) config.get("FilePath")),fileName,(String) config.get("SaveMode"));
    }
    /**
     * 根据传入的参数获取对应的数据.
     * @param dateTime 当前时间.
     * @param config 配置信息.
     * @return 一个包含配置文件所需信息的String.
     */
    private String getInformation(LocalDateTime dateTime,Map<String,Object> config) {
        StringBuilder stringBuilder = new StringBuilder();
        if ((boolean) config.get("UserName")) {stringBuilder.addString("用户名:",System.getProperty("user.name"));}
        if ((boolean) config.get("osName")) {stringBuilder.addString("系统:",System.getProperty("os.name"));}
        if ((boolean) config.get("HostName")) {
            try {stringBuilder.addString("主机名:",InetAddress.getLocalHost().getHostName());
            } catch (UnknownHostException e) {e.printStackTrace();}}
        if ((boolean) config.get("Time")) {
            stringBuilder.addString("启动时的时间:",dateTime.format(DateTimeFormatter.ofPattern((String) config.get("TimeFormatter"))));}
        if ((boolean) config.get("Date")) {stringBuilder.addString("启动时的日期:",dateTime.format(DateTimeFormatter.ofPattern((String) config.get("DateFormatter"))));}
        return stringBuilder.getResult();
    }
    /**
     * 获取配置文件的方法.
     * @return 配置文件内容的Map对象.
     */
    private Map<String,Object> getConfig() {
        //配置文件相关实例.
        String configPathString = JarPath + "config.yaml";
        Path configPath = Path.of(configPathString);
        //创建配置文件.
        if (Files.notExists(configPath, LinkOption.NOFOLLOW_LINKS)) {
            try {
                Files.createFile(configPath);
                Files.copy(ClassLoader.getSystemResourceAsStream("resources/defaultConfig.yaml"),configPath, StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {e.printStackTrace();}
        }
        Map<String,Object> map = null;
        try {map = new Yaml(new SafeConstructor()).load(new FileInputStream(configPathString));
        } catch (FileNotFoundException e) {e.printStackTrace();}
        //判断确定是否是正确的配置文件,如果是正确的则直接返回,如果不正确则删除config.yaml并递归调用此方法.
        if ("DenvosBootMonitor".equals(map.get("Program")) && "0.0.1".equals(map.get("Version"))) {return map;}
        else {
            try {Files.delete(configPath);} catch (IOException e) {e.printStackTrace();}
            return getConfig();
        }
    }
    /**
     * 将配置文件中的文件的位置转换成能用的位置.
     * @param s 配置文件中写的文件的位置.
     * @return 文件的位置.
     */
    private String getFilePath(String s) {
        if ("appPath".equals(s)) {return JarPath;}
        else {return s + "/";}
    }
    /**
     * 将配置文件中的FileName转换.
     * @param s 配置文件中的FileName.
     * @param dateTime 时间.
     * @return 能用的FileName.
     */
    private String getFileName(String s,LocalDateTime dateTime) {
        String s1 = s;
        s1 = s1.replaceAll("\\{time\\}",dateTime.format(DateTimeFormatter.ofPattern("HH.mm.ss")));
        s1 = s1.replaceAll("\\{date\\}",dateTime.format(DateTimeFormatter.ofPattern("yy.MM.dd")));
        return s1;
    }
    /**
     * 在./Report文件夹中新建一个文件,将传入的字符串保存进这个文件中.
     * @param s 需要保存的字符串.
     * @param fileName 保存的文件名.
     * @param filePath 保存的文件路径.
     * @param saveMode 保存模式,见配置文件或Wiki.
     */
    private void saveFile(String s,String filePath,String fileName,String saveMode) {
        boolean mode = false;
        String s1 = "";
        if ("continue".equals(saveMode)) {
            mode = true;
            s1 = "\n====================\n" + s;
        }
        File file = new File(filePath + fileName + ".txt");
        if (!file.exists()) {
            try {
                file.createNewFile();
                s1 = s;
            } catch (IOException e) {e.printStackTrace();}
        }
        try (OutputStream out = new FileOutputStream(file,mode)) {
            out.write(s1.getBytes());
            out.flush();
        } catch (IOException e) {e.printStackTrace();}
    }
}