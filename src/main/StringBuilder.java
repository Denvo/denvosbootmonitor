package main;

public class StringBuilder {
    private final java.lang.StringBuilder stringBuilder = new java.lang.StringBuilder();
    public void addString(String... s) {
        for (String string : s) {
            stringBuilder.append(string);
        }
        stringBuilder.append("\n");
    }
    public String getResult() {
        return stringBuilder.deleteCharAt(stringBuilder.length() - 1).toString();
    }
}